window.addEventListener('load', () => {

    // Dark Sky data download and DOM update
    let currentTemp = document.querySelector('.current-temp');
    const proxy = 'https://cors-anywhere.herokuapp.com/';
    let darksky_api = `${proxy}https://api.darksky.net/forecast/fd9c75d2d64d3318be118c5baaa6fc35/29.749907,-95.358421`;

    fetch(darksky_api)
        .then(response => {
            return response.json();
        })
        .then(data => {


            //Pull Just Current Temperature and configure DOM
            const {temperature} = data.currently
            currentTemp.textContent = Math.round(temperature) + '°F';
        
        })


})